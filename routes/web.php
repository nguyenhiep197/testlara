<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    // dd(app()); // Dòng code để kiểm chứng việc đã bind thành công

});
Route::get('/admin', function () {
    return view('admin')->with('name', 'Lê Chí Huy');

});

Route::get('/content', function () {
    return view('child')->with('name', 'Lê Chí Huy');

});

Route::redirect('/here', '/th2ere');

Route::get('/home',[HomeController::class,'show']);

Route::get('/download', function() {
    return response()->download('../resources/views/welcome.blade.php');
});

Route::get('/dashboard',[DashboardController::class,'view']);
